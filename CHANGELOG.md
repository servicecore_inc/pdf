# Changelog

All notable changes to this project will be documented in this file, in reverse chronological order by release.

## 4.0.1 - 2024-03-13

### Changed

- Update minimum allowed knplabs/knpsnappy version to 1.4.3 to address [CVE](https://github.com/KnpLabs/snappy/security/advisories/GHSA-92rv-4j2h-8mjj)

## 4.0.0 - 2023-05-25

### Changed

- Create context now has request injected into it
- Create context getHtml() now sets tz in params consistently

## 3.0.4 - 2023-05-25

### Added

- tz parameter added to viewmodels that represents the X-TZ header passed in from the request

## 3.x.x - 2023-05-24

### Added

- Versioning
