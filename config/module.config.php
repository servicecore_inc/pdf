<?php

use ServiceCore\Pdf\Config\Create as CreateConfig;
use ServiceCore\Pdf\Config\Factory\Create as CreateConfigFactory;
use ServiceCore\Pdf\Context\Create as PdfCreateContext;
use ServiceCore\Pdf\Context\Factory\Create as PdfCreateContextFactory;
use ServiceCore\Pdf\View\Factory\PhoneFormat as PhoneFormatViewFactory;
use ServiceCore\Pdf\View\PhoneFormat as PhoneFormatView;

return [
    'service_manager' => [
        'factories' => [
            PdfCreateContext::class => PdfCreateContextFactory::class,
            CreateConfig::class     => CreateConfigFactory::class,
        ]
    ],

    'view_helpers' => [
        'factories' => [
            PhoneFormatView::class => PhoneFormatViewFactory::class
        ],
        'aliases'   => [
            'phoneFormat' => PhoneFormatView::class
        ]
    ],

    'pdf' => [
        'path' => \sys_get_temp_dir()
    ],

    'labelTemplates' => [
        'Avery5160' => [
            'info'       => [
                'brand'       => 'Avery',
                'paperType'   => 'US Letter',
                'description' => 'Easy Peel Address Labels',
                'code'        => '5160',
                'size'        => '1 x 2 5/8',
            ],
            'dimensions' => [
                'units'        => 'in',
                'panel'        => [
                    'width'        => 2.625,
                    'height'       => 0.95,
                    'shape'        => 'rect',
                    'cornerRadius' => 0.094,
                    'margin'       => [
                        'horizontal' => 0.35,
                        'vertical'   => 0.0,
                    ],
                ],
                'panelWrapper' => [
                    'width'  => 3.09,
                    'height' => 1.28,
                ],
                'layout'       => [
                    'numberAcross' => 3,
                    'numberDown'   => 10,
                ],
                'page'         => [
                    'margin' => [
                        'horizontal' => 0.188,
                        'vertical'   => 0.05,
                    ],
                ],
            ],
        ],
        'Avery5161' => [
            'info'       => [
                'brand'       => 'Avery',
                'paperType'   => 'US Letter',
                'description' => 'Easy Peel Address Labels',
                'code'        => '5161',
                'size'        => '1 x 4',
            ],
            'dimensions' => [
                'units'        => 'in',
                'panel'        => [
                    'width'        => 4,
                    'height'       => .90,
                    'shape'        => 'rect',
                    'cornerRadius' => 0.094,
                    'margin'       => [
                        'horizontal' => 0.2,
                        'vertical'   => 0.0,
                    ],
                ],
                'panelWrapper' => [
                    'width'  => 4.75,
                    'height' => 1.28,
                ],
                'layout'       => [
                    'numberAcross' => 2,
                    'numberDown'   => 10,
                ],
                'page'         => [
                    'margin' => [
                        'horizontal' => 0.188,
                        'vertical'   => 0.05,
                    ],
                ],
            ],
        ],
        'Avery5162' => [
            'info'       => [
                'brand'       => 'Avery',
                'paperType'   => 'US Letter',
                'description' => 'Easy Peel Address Labels',
                'code'        => '5162',
                'size'        => '1 1/3 x 4',
            ],
            'dimensions' => [
                'units'        => 'in',
                'panel'        => [
                    'width'        => 4,
                    'height'       => 1.2,
                    'shape'        => 'rect',
                    'cornerRadius' => 0.094,
                    'margin'       => [
                        'horizontal' => 0.2,
                        'vertical'   => 0.0,
                    ],
                ],
                'panelWrapper' => [
                    'width'  => 4.70,
                    'height' => 1.70,
                ],
                'layout'       => [
                    'numberAcross' => 2,
                    'numberDown'   => 7,
                ],
                'page'         => [
                    'margin' => [
                        'horizontal' => 0.188,
                        'vertical'   => 0.55,
                    ],
                ],
            ],
        ],
        'Avery5163' => [
            'info'       => [
                'brand'       => 'Avery',
                'paperType'   => 'US Letter',
                'description' => 'Shipping Labels',
                'code'        => '5163',
                'size'        => '2 x 4',
            ],
            'dimensions' => [
                'units'        => 'in',
                'panel'        => [
                    'width'        => 4,
                    'height'       => 1.5,
                    'shape'        => 'rect',
                    'cornerRadius' => 0.094,
                    'margin'       => [
                        'horizontal' => 0.2,
                        'vertical'   => 0.0,
                    ],
                ],
                'panelWrapper' => [
                    'width'  => 4.75,
                    'height' => 2.52,
                ],
                'layout'       => [
                    'numberAcross' => 2,
                    'numberDown'   => 5,
                ],
                'page'         => [
                    'margin' => [
                        'horizontal' => 0.188,
                        'vertical'   => 0.2,
                    ],
                ],
            ],
        ]
    ]
];
