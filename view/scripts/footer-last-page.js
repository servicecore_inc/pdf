function substitute()
{
    var vars = {};

    // explode the URL query string
    var queryArray = document.location.search.substring(1).split('&');

    // loop through each query string segment and get keys/values
    for (var i in queryArray) {
        var z = queryArray[i].split('=',2);
        vars[z[0]] = decodeURI(z[1]);
    }

    // an array of all the parameters passed into the footer file
    var x = ['frompage', 'topage', 'page', 'webpage', 'section', 'subsection', 'subsubsection'];

    // each page will have an element with class 'section' from the body of the footer HTML
    var y = document.getElementsByClassName('section');

    for (var j = 0; j < y.length; j++) {
        // if current page equals total pages
        if (vars[x[2]] == vars[x[1]]) {
            y[j].innerHTML = document.getElementById('footer').innerHTML;
            y[j].innerHTML.css('display', 'block');
        }
    }
}
