<?php

namespace ServiceCore\Pdf\Test\Delegator\Factory;

use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\ServiceManager;
use PHPUnit\Framework\TestCase;
use ServiceCore\Pdf\Context\Create;
use ServiceCore\Pdf\Delegator\Factory\CreatePdf;
use ServiceCore\Pdf\Helper\CreatePdfAware;
use ServiceCore\Pdf\RoleData\PdfCreatable;
use stdClass;

class CreatePdfTest extends TestCase
{
    public function testInvokeThrowsExceptionIfNotPdfCreatable(): void
    {
        $delegator = new CreatePdf();
        $callable  = function () {
            return new class() {};
        };

        $container = $this->createMock(ServiceManager::class);

        $container->expects($this->never())
                  ->method('get');

        $this->expectException(ServiceNotCreatedException::class);

        $delegator($container, stdClass::class, $callable);
    }

    public function testInvoke(): void
    {
        $delegator = new CreatePdf();
        $callable  = function () {
            return new class() implements  PdfCreatable {
                use CreatePdfAware;
            };
        };

        $container        = $this->createMock(ServiceManager::class);
        $pdfCreateContext = $this->createMock(Create::class);
        $labelTemplates   = [];

        $container->expects($this->exactly(2))
                  ->method('get')
                  ->willReturnOnConsecutiveCalls($pdfCreateContext, $labelTemplates);

        $creatable = $delegator($container, stdClass::class, $callable);

        $this->assertSame($pdfCreateContext, $creatable->getPdfCreateContext());
    }
}
