<?php

namespace ServiceCore\Pdf;

use PHPUnit\Framework\TestCase;

class ModuleTest extends TestCase
{
    public function testGetConfig(): void
    {
        $module = new Module();
        $config = $module->getConfig();

        $this->assertArrayHasKey('service_manager', $config);
        $this->assertArrayHasKey('view_helpers', $config);
        $this->assertArrayHasKey('pdf', $config);
        $this->assertArrayHasKey('labelTemplates', $config);
    }
}
