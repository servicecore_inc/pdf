<?php

namespace ServiceCore\Pdf\Test\Helper;

use Laminas\Http\Header\Accept;
use Laminas\Http\Header\GenericHeader;
use Laminas\Http\Request;
use Laminas\Http\Response;
use Laminas\Paginator\Paginator;
use Laminas\Stdlib\FastPriorityQueue;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use ServiceCore\Pdf\Context\Create;
use ServiceCore\Pdf\Data\Provider;
use ServiceCore\Pdf\Helper\CreatePdfAware;

class CreatePdfAwareTest extends TestCase
{
    public function testFetchEntityOrPdf(): void
    {
        /** @var CreatePdfAware|MockObject $mock */
        $mock = $this->getMockForTrait(
            CreatePdfAware::class,
            [],
            '',
            true,
            true,
            true,
            [
                'getPdfCreateContext'
            ]
        );

        $pdfCreate = $this->createMock(Create::class);
        $accept    = $this->createMock(Accept::class);
        $timezone  = $this->createMock(GenericHeader::class);

        $timezone->expects($this->once())
                 ->method('getFieldValue')
                 ->willReturn('fooBar');

        $pdfCreate->expects($this->once())
                  ->method('isRequestingPdf')
                  ->with($accept)
                  ->willReturn(true);

        $mock->expects($this->once())
             ->method('getPdfCreateContext')
             ->willReturn($pdfCreate);

        $entity  = $this->getMockProvider();
        $request = $this->createMock(Request::class);

        $request->expects($this->exactly(2))
                ->method('getHeader')
                ->withConsecutive(['Accept'], ['X-TZ'])
                ->willReturnOnConsecutiveCalls($accept, $timezone);

        $this->assertInstanceOf(Response::class, $mock->fetchEntityOrPdf($entity, $request));
    }

    public function testFetchEntitiesOrPdf(): void
    {
        /** @var CreatePdfAware|MockObject $mock */
        $mock = $this->getMockForTrait(
            CreatePdfAware::class,
            [],
            '',
            true,
            true,
            true,
            [
                'getPdfCreateContext'
            ]
        );

        $pdfCreate = $this->createMock(Create::class);
        $accept    = $this->createMock(Accept::class);
        $timezone  = $this->createMock(GenericHeader::class);

        $timezone->expects($this->once())
                 ->method('getFieldValue')
                 ->willReturn('fooBar');

        $pdfCreate->expects($this->once())
                  ->method('isRequestingPdf')
                  ->with($accept)
                  ->willReturn(true);

        $mock->expects($this->exactly(2))
             ->method('getPdfCreateContext')
             ->willReturn($pdfCreate);

        $entities = $this->createMock(Paginator::class);

        $entities->expects($this->once())
                 ->method('count')
                 ->willReturn(1);

        $entities->expects($this->once())
                 ->method('setItemCountPerPage')
                 ->willReturnSelf();

        $entities->expects($this->once())
                 ->method('getTotalItemCount')
                 ->willReturn(1);

        $iterator = new FastPriorityQueue();

        $entities->expects($this->once())
                 ->method('getIterator')
                 ->willReturn($iterator);

        $request = $this->createMock(Request::class);

        $request->expects($this->exactly(2))
                ->method('getHeader')
                ->withConsecutive(['Accept'], ['X-TZ'])
                ->willReturnOnConsecutiveCalls($accept, $timezone);

        $callable = function ($entities) {
            $this->assertInstanceOf(Response::class, $entities);
        };

        $params = [
            'perPage' => 1
        ];

        $mock->fetchEntitiesOrPdf($entities, $request, $params, $callable);
    }

    private function getMockProvider(): Provider
    {
        return new class implements Provider {
            public function getPdfFilename(): string
            {
                return 'foo.pdf';
            }

            public function getPdfTemplate(): string
            {
                return '<html></html>';
            }

            public function getPdfTemplateFooter(): ?string
            {
                return null;
            }
        };
    }
}
