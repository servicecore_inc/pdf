<?php

namespace ServiceCore\Pdf\Test\Exception;

use PHPUnit\Framework\TestCase;
use ServiceCore\Pdf\Exception\CreateFromResponse;

class CreateFromResponseTest extends TestCase
{
    public function testPdfNotDefinedInConfig(): void
    {
        $exception = new CreateFromResponse();
        $routeName = 'foo/bar';
        $pdfName   = 'fooBar';
        $exception = $exception::pdfNotDefinedInConfig($routeName, $pdfName);
        $message   = $exception->getMessage();

        $this->assertStringContainsString($routeName, $message);
        $this->assertStringContainsString($pdfName, $message);
    }
}
