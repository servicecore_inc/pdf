<?php

namespace ServiceCore\Pdf\Test\Config;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use ServiceCore\Pdf\Config\Create;

/**
 * @group create
 */
class CreateTest extends TestCase
{
    public function testDefaultValues(): void
    {
        $config = new Create();

        $this->assertStringContainsString('/bin/wkhtmltopdf-amd64', $config->getBinary());
        $this->assertStringContainsString(\sys_get_temp_dir(), $config->getPath());
    }

    public function testSetPath(): void
    {
        $config = new Create();
        // setPath appends a / onto the end if it doesn't exist, so to make the assertEquals() call work, put one here
        $path = __DIR__ . '/';

        $config->setPath($path);

        $this->assertEquals($path, $config->getPath());
    }

    public function testSetPathAppendsSlash(): void
    {
        $config = new Create();
        $path = __DIR__;

        $config->setPath($path);

        $this->assertEquals($path . '/', $config->getPath());
    }

    public function testSetPathThrowsExceptionIfPathDoesNotExist(): void
    {
        $config = new Create();

        $this->expectException(InvalidArgumentException::class);

        $config->setPath('/path/does/not/exist');
    }

    public function testGetPdfOptions(): void
    {
        $config  = new Create();
        $options = $config->getPdfOptions();

        $this->assertArrayHasKey('lowquality', $options);
        $this->assertArrayHasKey('page-size', $options);
    }
}
