<?php

namespace ServiceCore\Pdf\Test\Config\Factory;

use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\ServiceManager;
use PHPUnit\Framework\TestCase;
use ServiceCore\Pdf\Config\Create;
use ServiceCore\Pdf\Config\Factory\Create as Factory;

/**
 * @group factory
 * @group create
 */
class CreateTest extends TestCase
{
    public function testInvokeThrowsExceptionIfPdfKeyMissing(): void
    {
        $factory   = new Factory();
        $container = $this->createMock(ServiceManager::class);
        $config    = [];

        $container->expects($this->once())
                  ->method('get')
                  ->with('config')
                  ->willReturn($config);

        $this->expectException(ServiceNotCreatedException::class);

        $factory($container, Create::class);
    }

    public function testInvoke(): void
    {
        $factory   = new Factory();
        $container = $this->createMock(ServiceManager::class);
        $config    = [
            'pdf' => []
        ];

        $container->expects($this->once())
                  ->method('get')
                  ->with('config')
                  ->willReturn($config);

        $factory($container, Create::class);
    }
}
