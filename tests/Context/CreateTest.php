<?php

namespace ServiceCore\Pdf\Test\Context;

use Composer\Autoload\ClassLoader;
use Laminas\Http\Header\Accept;
use Laminas\Http\Response\Stream;
use Laminas\Http\Request;
use Laminas\View\Renderer\PhpRenderer;
use PHPUnit\Framework\TestCase;
use ReflectionClass;
use ServiceCore\Pdf\Config\Create as CreateOptions;
use ServiceCore\Pdf\Context\Create;
use ServiceCore\Pdf\Data\Provider;

/**
 * @group context
 * @group create
 */
class CreateTest extends TestCase
{
    public function testMakeResponse(): void
    {
        $createOptions = $this->getCreateOptions();
        $timeout       = 2;
        $renderer      = $this->createMock(PhpRenderer::class);
        $request       = $this->createMock(Request::class);
        $create        = new Create($createOptions, $renderer, $request);
        $data          = [$this->getMockProvider()];
        $params        = ['landscape' => true, 'timeout' => $timeout];
        $response      = $create->makeResponse($data, $params);

        $this->assertInstanceOf(Stream::class, $response);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertNotEmpty($response->getHeaders());

        $resource = $response->getStream();

        \ob_start();
        $this->assertStringContainsString('PDF-1.4', \stream_get_contents($resource));
        \ob_get_clean();
    }

    public function testIsRequestingPdfReturnsFalse(): void
    {
        $createOptions = new CreateOptions();
        $renderer      = $this->createMock(PhpRenderer::class);
        $request       = $this->createMock(Request::class);
        $create        = new Create($createOptions, $renderer, $request);
        $accept        = $this->createMock(Accept::class);

        $accept->expects($this->once())
               ->method('getFieldValue')
               ->willReturn('application/html');

        $this->assertFalse($create->isRequestingPdf($accept));
    }

    public function testIsRequestingPdfReturnsTrue(): void
    {
        $createOptions = new CreateOptions();
        $renderer      = $this->createMock(PhpRenderer::class);
        $request       = $this->createMock(Request::class);
        $create        = new Create($createOptions, $renderer, $request);
        $accept        = $this->createMock(Accept::class);

        $accept->expects($this->once())
               ->method('getFieldValue')
               ->willReturn('application/pdf');

        $this->assertTrue($create->isRequestingPdf($accept));
    }

    public function testGetSplFileInfo(): void
    {
        $createOptions = $this->getCreateOptions();
        $renderer      = $this->createMock(PhpRenderer::class);
        $request       = $this->createMock(Request::class);
        $create        = new Create($createOptions, $renderer, $request);
        $data          = [$this->getMockProvider()];

        $file = $create->getSplFileInfo($data);

        $this->assertStringContainsString('pdf_', $file->getFilename());
    }

    private function getMockProvider(): Provider
    {
        return new class implements Provider
        {
            public function getPdfFilename(): string
            {
                return 'filename';
            }

            public function getPdfTemplate(): string
            {
                return <<<HTML
<html>
    <body>
        <h1>A Pdf!</h1>
    </body>
</html>
HTML;
            }

            public function getPdfTemplateFooter(): ?string
            {
                return null;
            }
        };
    }

    private function getCreateOptions(): CreateOptions
    {
        $reflection = new ReflectionClass(ClassLoader::class);
        $vendorDir  = \dirname($reflection->getFileName(), 2);

        return new CreateOptions(['binary' => $vendorDir . '/bin/wkhtmltopdf-amd64']);
    }
}
