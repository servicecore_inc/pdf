<?php

namespace ServiceCore\Pdf\Test\Context\Factory;

use Laminas\Http\Request;
use Laminas\ServiceManager\ServiceManager;
use Laminas\View\Renderer\PhpRenderer;
use PHPUnit\Framework\TestCase;
use ServiceCore\Pdf\Config\Create as CreateConfig;
use ServiceCore\Pdf\Context\Create as CreateContext;
use ServiceCore\Pdf\Context\Factory\Create as CreateFactory;

/**
 * @group create
 */
class CreateTest extends TestCase
{
    public function testInvoke(): void
    {
        $factory      = new CreateFactory();
        $container    = $this->createMock(ServiceManager::class);
        $phpRenderer  = $this->createMock(PhpRenderer::class);
        $createConfig = $this->createMock(CreateConfig::class);
        $request      = $this->createMock(Request::class);

        $container->expects($this->once())
                  ->method('has')
                  ->willReturn(true);

        $container->expects($this->exactly(3))
                  ->method('get')
                  ->willReturn($phpRenderer, $createConfig, $request);

        $factory($container, CreateContext::class);
    }
}
