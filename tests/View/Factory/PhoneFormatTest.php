<?php

namespace ServiceCore\Pdf\Test\View\Factory;

use Laminas\ServiceManager\ServiceManager;
use PHPUnit\Framework\TestCase;
use ServiceCore\Pdf\View\Factory\PhoneFormat as Factory;
use ServiceCore\Pdf\View\PhoneFormat as Service;

class PhoneFormatTest extends TestCase
{
    public function testInvoke(): void
    {
        $services = new ServiceManager();

        $this->assertInstanceOf(Service::class, (new Factory())($services, ''));
    }
}
