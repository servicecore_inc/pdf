<?php

namespace ServiceCore\Pdf\Test\View;

use PHPUnit\Framework\TestCase;
use ServiceCore\Pdf\View\PhoneFormat;

class PhoneFormatTest extends TestCase
{
    public function testInvokeReturnsStringIfPhoneIsNull(): void
    {
        $this->assertEquals('', (new PhoneFormat())(null));
    }

    public function testInvokeReturnsStringIfPhoneIsEmpty(): void
    {
        $this->assertEquals('', (new PhoneFormat())(''));
    }

    public function testInvokeReturnsStringIfPhoneIsInvalid(): void
    {
        $this->assertEquals('', (new PhoneFormat())('foo'));
    }

    public function testInvokeReturnsStringIfPhoneDoesNotHaveExtension(): void
    {
        $this->assertEquals('(234) 567-8901', (new PhoneFormat())('+12345678901'));
    }

    public function testInvokeReturnsStringIfPHoneDoesHaveExtension(): void
    {
        $this->assertEquals(
            '(234) 567-8901 ext. 234', 
            (new PhoneFormat())('+12345678901 ext. 234')
        );
    }
}
