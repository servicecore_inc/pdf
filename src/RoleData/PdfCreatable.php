<?php

namespace ServiceCore\Pdf\RoleData;

use ServiceCore\Pdf\Context\Create as PdfCreateContext;

interface PdfCreatable
{
    public function setPdfCreateContext(PdfCreateContext $create);

    public function getPdfCreateContext(): PdfCreateContext;

    public function setLabelTemplates(?array $labelTemplates = null);
}
