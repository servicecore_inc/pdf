<?php

namespace ServiceCore\Pdf\Data;

interface Provider
{
    public function getPdfFilename(): string;

    public function getPdfTemplate(): string;

    public function getPdfTemplateFooter(): ?string;
}
