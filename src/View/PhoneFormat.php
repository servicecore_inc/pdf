<?php

namespace ServiceCore\Pdf\View;

use Laminas\View\Helper\AbstractHelper;

/**
 * The phone-format view helper
 * I'll take a properly formatted US e164 phone number and format it as a human-
 * friendly phone number:
 *     $service = new PhoneFormat();
 *     $service(null);                     // returns ""
 *     $service('123');                    // returns ""
 *     $service('+12345678901');           // returns "(234) 567-8901"
 *     $service('+12345678901 ext. 123');  // returns "(234) 567-8901 ext. 123"
 */
class PhoneFormat extends AbstractHelper
{
    public function __invoke(string $phone = null): string
    {
        // if $phone is empty, short-circuit
        if ($phone === '') {
            return '';
        }

        // if $phone is not US e64, short-circuit
        if (!\preg_match('/^\+1[0-9]{10}( ext\. [0-9]+)?$/', $phone)) {
            return '';
        }

        // otherwise, drop the "+1" prefix
        // e.g., "+1##########" -> "##########"
        $phone = \substr($phone, 2);

        // split the number and the extension (this is non-extension safe)
        // e.g., "########## ext. ###" -> "##########" + " ext. ###"
        $number = \substr($phone, 0, 10);
        $suffix = \substr($phone, 10);

        // format the number
        // e.g., "##########" -> "(###) ###-####"
        $number = '('
            . \substr($number, 0, 3)
            . ') '
            . \substr($number, 3, 3)
            . '-'
            . \substr($number, 6);

        // append the suffix (if it exists)
        $phone = $number . $suffix;

        return $phone;
    }
}
