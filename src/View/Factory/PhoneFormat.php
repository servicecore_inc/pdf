<?php

namespace ServiceCore\Pdf\View\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use ServiceCore\Pdf\View\PhoneFormat as Service;

class PhoneFormat implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ): Service {
        return new Service();
    }
}
