<?php

namespace ServiceCore\Pdf\Context\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Laminas\View\Renderer\PhpRenderer;
use ServiceCore\Pdf\Config\Create as CreateConfig;
use ServiceCore\Pdf\Context\Create as CreateContext;

class Create implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): CreateContext
    {
        /** @var PhpRenderer $renderer */
        $renderer = $container->get('ViewRenderer');
        $config   = $container->get(CreateConfig::class);

        if (!$container->has('Request')) {
            throw new ServiceNotFoundException();
        }

        $request = $container->get('Request');

        return new CreateContext(
            $config,
            $renderer,
            $request
        );
    }
}
