<?php

namespace ServiceCore\Pdf\Context;

use Knp\Snappy\Pdf as SnappyPdf;
use Laminas\Http\Header\Accept;
use Laminas\Http\Header\CacheControl;
use Laminas\Http\Header\ContentDisposition;
use Laminas\Http\Header\ContentLength;
use Laminas\Http\Header\ContentType;
use Laminas\Http\Header\Expires;
use Laminas\Http\Header\Pragma;
use Laminas\Http\Headers;
use Laminas\Http\Request;
use Laminas\Http\Response;
use Laminas\View\Model\ViewModel;
use Laminas\View\Renderer\PhpRenderer;
use ServiceCore\Pdf\Config\Create as CreateConfig;
use ServiceCore\Pdf\Data\Provider;
use SplFileInfo;

class Create
{
    private CreateConfig $options;

    private PhpRenderer $renderer;

    private SnappyPdf $file;

    private string $outputFile = '';

    private Request $request;

    public function __construct(
        CreateConfig $options,
        PhpRenderer $renderer,
        Request $request
    ) {
        $this->options  = $options;
        $this->renderer = $renderer;
        $this->request  = $request;
        $this->file     = new SnappyPdf(
            $options->getBinary(),
            $options->getPdfOptions()
        );
    }

    public function __destruct()
    {
        if (\file_exists($this->outputFile)) {
            \unlink($this->outputFile);
        }
    }

    public function makeResponse($data, array $params = []): Response
    {
        $splFileInfo = $this->getSplFileInfo($data, $params);
        $headers     = new Headers();

        $headers->addHeader(new ContentDisposition(\sprintf('attachment; filename="%s"', $splFileInfo->getBasename())));
        $headers->addHeader(new ContentType('application/octet-stream'));
        $headers->addHeader(new ContentLength($splFileInfo->getSize()));
        $headers->addHeader(new Expires());
        $headers->addHeader(new CacheControl());
        $headers->addHeader(new Pragma('public'));

        $response = new Response\Stream();
        $response->setStream(\fopen($splFileInfo->getRealPath(), 'rb'));
        $response->setStreamName(\basename($splFileInfo->getBasename()));
        $response->setHeaders($headers);
        $response->setStatusCode(200);

        return $response;
    }

    public function isRequestingPdf(Accept $accept): bool
    {
        return \strpos('application/pdf', $accept->getFieldValue()) !== false;
    }

    public function getSplFileInfo($data, array $params = []): SplFileInfo
    {
        $datum            = \is_array($data) ? \end($data) : $data;
        $this->outputFile = $this->options->getPath() . \str_replace('.', '', \uniqid('pdf_', true)) . '.pdf';

        $this->setHeader($datum)
             ->setFooter($datum)
             ->getFile($data, $params);

        return new SplFileInfo($this->outputFile);
    }

    private function getHtml($data, array $params = []): string
    {
        if (!\is_array($data)) {
            $data = [$data];
        }

        if (\array_key_exists('landscape', $params) && $params['landscape']) {
            $this->file->setOption('orientation', 'Landscape');
        }

        $timezoneHeader = $this->request->getHeader('X-TZ');
        $params['tz']   = $timezoneHeader ? $timezoneHeader->getFieldValue() : null;

        $result    = '';
        $dataCount = \count($data);

        /** @var Provider $datum */
        foreach ($data as $key => $datum) {
            $viewModel = new ViewModel(
                [
                    'entity'      => $datum,
                    'isFirstPage' => $key === 0,
                    'isLastPage'  => $key === $dataCount - 1,
                    'params'      => $params,
                    'page'        => $key + 1,
                ]
            );

            $viewModel->setTerminal(true);
            $viewModel->setTemplate($datum->getPdfTemplate());

            $result .= $this->renderer->render($viewModel);

            $footer = $datum->getPdfTemplateFooter();

            if ($footer && $this->renderer->resolver($footer)) {
                $viewModel->setTerminal(true);
                $viewModel->setTemplate($footer);

                $result .= $this->renderer->render($viewModel);
            }

            unset($data[$key]);
        }

        unset($dataCount);

        return $result;
    }

    private function getFile($data, array $params = []): SnappyPdf
    {
        if (\array_key_exists('timeout', $params) && \is_int($params['timeout'])) {
            $this->file->setTimeout($params['timeout']);
        }

        $this->file->generateFromHtml($this->getHtml($data, $params), $this->outputFile);

        return $this->file;
    }

    private function setHeader(Provider $data): self
    {
        return $this;
    }

    private function setFooter(Provider $data): self
    {
        $view = new ViewModel(
            [
                'entity' => $data,
            ]
        );

        $footerName = $data->getPdfTemplateFooter();

        if ($footerName && $this->renderer->resolver($footerName)) {
            $view->setTerminal(true);
            $view->setTemplate($footerName);

            $this->file->setOption('footer-html', $this->renderer->render($view));
        }

        return $this;
    }
}
