<?php

namespace ServiceCore\Pdf\Transformer;

interface TransformerInterface
{
    public function transform($payload): array;
}
