<?php

namespace ServiceCore\Pdf\Exception;

use Exception;

class CreateFromResponse extends Exception
{
    public static function pdfNotDefinedInConfig(string $routeName, string $pdfName): Exception
    {
        return new self(\sprintf(
            'The requested route name and PDF name were not defined in the config. Requested `%s` and `%s`.',
            $routeName,
            $pdfName
        ));
    }
}
