<?php

namespace ServiceCore\Pdf\Helper;

use Laminas\Http\Header\Accept;
use Laminas\Http\Request;
use Laminas\Http\Response;
use Laminas\Paginator\Paginator;
use ServiceCore\Pdf\Context\Create as CreateContext;
use ServiceCore\Pdf\Data\Provider;

trait CreatePdfAware
{
    /** @var CreateContext */
    private $createPdfContext;

    /** @var int */
    private $perPage = 20;

    /** @var array */
    private $labelTemplates;

    /**
     * @Deprecated
     */
    public function fetchEntityOrPdf(Provider $entity, Request $request, array $params = [])
    {
        $context = $this->getPdfCreateContext();
        $accept  = $request->getHeader('Accept');

        if ($accept instanceof Accept && $context->isRequestingPdf($accept)) {
            $timezoneHeader = $request->getHeader('X-TZ');
            $params['tz']   = $timezoneHeader ? $timezoneHeader->getFieldValue() : null;
            $entity         = $context->makeResponse($entity, $params);
        }

        return $entity;
    }

    /**
     * @Deprecated
     */
    public function fetchEntitiesOrPdf(
        Paginator $entities,
        Request $request,
        array $params = [],
        callable $pdfCallback = null
    ) {
        $context = $this->getPdfCreateContext();
        $accept  = $request->getHeader('Accept');

        if ($accept instanceof Accept
            && $context->isRequestingPdf($accept)
            && $entities->count() > 0) {
            $sortedEntities = null;

            if (\array_key_exists('perPage', $params)) {
                $this->setPdfPerPage($params['perPage']);
            }

            $entities->setItemCountPerPage($entities->getTotalItemCount());

            if (\array_key_exists('filter', $params) && \is_array($params['filter'])) {
                $filtered = \array_filter($params['filter'], function (array $filter) {
                    return \array_key_exists('type', $filter) && $filter['type'] === 'in';
                });

                if ($filtered) {
                    $sortedEntities = $this->sortEntities($entities, \reset($filtered)['values'] ?? []);
                }
            }

            $timezoneHeader = $request->getHeader('X-TZ');
            $params['tz']   = $timezoneHeader ? $timezoneHeader->getFieldValue() : null;
            $entities       = $this->getPdfResponse(
                $sortedEntities ?: \iterator_to_array($entities->getIterator()),
                $params
            );

            if ($pdfCallback) {
                $pdfCallback($entities);
            }

            unset($sortedEntities);
        }

        return $entities;
    }

    public function setPdfCreateContext(CreateContext $create): self
    {
        $this->createPdfContext = $create;

        return $this;
    }

    public function getPdfCreateContext(): CreateContext
    {
        return $this->createPdfContext;
    }

    public function getPdfPerPage(): int
    {
        return $this->perPage;
    }

    public function setPdfPerPage(int $perPage): self
    {
        $this->perPage = $perPage;

        return $this;
    }

    public function getLabelTemplates(): ?array
    {
        return $this->labelTemplates;
    }

    public function setLabelTemplates(array $labelTemplates = null): self
    {
        $this->labelTemplates = $labelTemplates;

        return $this;
    }

    private function sortEntities(Paginator $entities, array $ids): array
    {
        $iterator = \iterator_to_array($entities->getIterator());
        $sorted   = [];

        foreach ($ids as $key => $id) {
            foreach ($iterator as $k => &$item) {
                if (\method_exists($item, 'getId') && $item->getId() === (int)$id) {
                    $sorted[] = $item;
                    break;
                }

                unset($item);
            }

            unset($ids[$key], $item);
        }

        return $sorted;
    }

    private function getPdfResponse(array $entities, array $params): Response
    {
        $context = $this->getPdfCreateContext();

        return $context->makeResponse($entities, $params);
    }
}
