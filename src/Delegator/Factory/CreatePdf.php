<?php

namespace ServiceCore\Pdf\Delegator\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use ServiceCore\Pdf\Context\Create as PdfCreateContext;
use ServiceCore\Pdf\RoleData\PdfCreatable;

class CreatePdf implements DelegatorFactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $name,
        callable $callback,
        array $options = null
    ): PdfCreatable {
        /** @var PdfCreatable $creatable */
        $creatable = $callback();

        if (!$creatable instanceof PdfCreatable) {
            throw new ServiceNotCreatedException(
                \sprintf(
                    'Class %s does not implement %s, can not use delegator.',
                    \get_class($creatable),
                    PdfCreatable::class
                )
            );
        }

        $creatable->setPdfCreateContext($container->get(PdfCreateContext::class));

        $creatable->setLabelTemplates($container->get('config')['labelTemplates'] ?? null);

        return $creatable;
    }
}
